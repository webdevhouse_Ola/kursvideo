<?php
    $obrazki = array(
        1 => 'okapi',
        2 => 'urson',
        3 => 'pingwiny',
        4 => 'kotiki',
        5 => 'mrownik'
    ) ;
    $indeks = mt_rand(1, count($obrazki));
    $zwierzeta = $obrazki[$indeks];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wrocławskie ZOO</title>
</head>
<body>
    <h1>Nasi podopieczni</h1>
    <a href="">
        <img src="obrazki/<?php echo $zwierzeta; ?>.jpg" alt="zwierze">
        <br>Zobacz galerię&gt;
    </a>
    
</body>
</html>