App: Ruby

gem install sass
sass style.scss ../css/style.css
sass --watch style.scss:../css/style.css --style compressed

gem install compass
compass create
compass compile / compass watch

compass_interactive

w pliku print.scss -> @import "compass";

http://compass-style.org/help/docu