/*

    Cookies - ciasteczka to kontenery przechowujące wartości w postaci:
                klucz=wartość;klucz2=wartość2;
              WARTOŚCI TE SĄ DOSTĘPNE DLA NAS NAWET GDY UŻYTKOWNIK OPUŚCI JĄ 
              przez co możemy identyfikować użytkownika przy ponownej wizycie


              path=/
              expires= - wygaszenie ważności ciastka domyślicznie po zakończeniu sesji (toUTCString())
              max-age= - maksymalny wiek cookie podany w sekundach (niewspierany w ie6-8)
              
              
 */

 function createCookie(name, value, days){

    var expires = "";

    if(days){
        var expirationDateOfCookie = new Date();
        expirationDateOfCookie.setDate(expirationDateOfCookie.getDate()+days);
        expires = ";expires=" + expirationDateOfCookie.toUTCString();
    }

    document.cookie = name + "=" + encodeURIComponent(value) + expires + ";patch=/";

 }

 function removeCookie(name){

     var expirationDateOfCookie = new Date();
     expirationDateOfCookie.setTime(expirationDateOfCookie.getTime()-1);
     document.cookie = name+"=;expires="+expirationDateOfCookie.toUTCString()+";path=/";
 }

 function getCookieByName(name){
     var cookies = document.cookie.split("; ");

     for(var i = 0; i < cookies.length; i++){
         var splittedCookie = cookies[i].split("=");
         var cookieName = splittedCookie[0];
         
         if(cookieName === name){
             var cookieValue = splittedCookie[1];
             return encodeURIComponent(cookieValue);
         }
     }

 }


window.onload = function()
{

    var info = document.getElementById("info");
    var stworzCiacha = document.getElementById("stworzCiacha");
    var usunCiacho = document.getElementById("usunCiacho");
    
    
    info.innerHTML = getCookieByName("imie");

    stworzCiacha.onclick = function()
    {
       createCookie("imie","Arkadiusz", 30);
       createCookie("nazwisko","Costam");
       //document.cookie = "imie=Arkadiusz;max-age="+60*60+";path=/";
       //document.cookie = "nazwisko=Wlodarczyk;max-age="+60*60+";path=/";
       
       
    };
    usunCiacho.onclick = function()
    {
       removeCookie("nazwisko");
    };
};
