/*
    keydown - gdy wciskamy dowolny klawisz
    keypress - gdy wciskamy klawisz znakowy (1,2,3,a,b,c, ', /)
    keyup  - gdy puścimy dowolny klawisz

    onchange - gdy user zmieni zawartość i zmieni input
    onfocus - gdzy user "wejdzie" do inputa
    onblur - gdy stracimy focus 
    e.which - znak klawisza
*/
function isNumber(valueToCheck) {
  return !isNaN(valueToCheck);
}

window.onload = function () {

  var poleLiczbowe = document.getElementById("myForm").poleLiczbowe;
  var poleTekstowe = document.getElementById("myForm").poleTekstowe;
  var info = document.getElementById("info");

  var isEverythingOk = true;

  poleLiczbowe.onkeydown = function (e) {

    var wpisanyZnak = e.which;
    

    //isNaN
    //if (!isNumber(this.value)) 
    if(isNumber(String.fromCharCode(wpisanyZnak)) || wpisanyZnak === 8 || wpisanyZnak === 190){      
      this.style.backgroundColor = "green";
      info.innerHTML = "";    
      isEverythingOk = true;  
    }
    else {
      e.preventDefault();
      this.style.backgroundColor = "red";
      info.innerHTML = "Niepoprawny fomat!";
      isEverythingOk = false;
    }


  };

  // submitMyForm.onclick = function(e){

  //   if(!isEverythingOk)
  //     e.preventDefault();
  // }

  //
  //
  //

  var myForm2 = document.getElementById("myForm2");
  var submitButton = document.getElementById("myForm2").submitButton;

  var info2 = document.getElementById("info2");

  submitButton.onclick = function(event){

    var tmpString ="";
    for(var i=0; i<myForm2.nazwaKursu.length; i++){

      if(myForm2.nazwaKursu[i].checked)
      tmpString += myForm2.nazwaKursu[i].value + " ";
    }
    info2.innerHTML += tmpString + "</br>";

    
    event.preventDefault();
  };

  for(var i = 0; i < myForm2.akceptacjaRegulaminu.length; i++){
    myForm2.akceptacjaRegulaminu[0].onclick = function(){
      submitButton.disabled = (this.value === "true");       
    }
  }

  //
  //
  //

  var myForm3 = document.getElementById("myForm3");
  var info3 = document.getElementById("info3");

  var newOption = document.createElement("option");
  newOption.text = "hej";

  myForm3.videoKursy.selectedIndex = 2;
  //alert(myForm3.videoKursy.options[1].value);

  myForm3.videoKursy.onchange = function(){

    info3.innerHTML = myForm3.videoKursy.options[myForm3.videoKursy.selectedIndex].value;
  };

};