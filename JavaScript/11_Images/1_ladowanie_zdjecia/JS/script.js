/*

    
              
              
 */

var i = 1;

function loadMoreImages() {

    if (i < 4) {

        var images = document.getElementById("images");
        var image = new Image();
        image.src = "images/screen" + (i++) + ".jpg";

        images.appendChild(image);

        if (i !== 4) {
            var buttonLoadMoreImagesClone = this.cloneNode(true);
            buttonLoadMoreImagesClone.onclick = loadMoreImages;

            document.body.appendChild(buttonLoadMoreImagesClone);
        }

    }
    this.parentNode.removeChild(this);


}

window.onload = function () {

    var loadMoreButton = document.getElementById("loadMoreButton");

    loadMoreButton.onclick = loadMoreImages;


};
