/*
   Galeria obrazków z miniaturkami
    
 */
function createTooltips() {

    var elementsWithTooltips = document.getElementsByClassName("tooltip");

    var toolTipContainer = document.createElement("div");
    toolTipContainer.id = "tooltipsContainer";
    document.body.appendChild(toolTipContainer);

    var tmpTitles = [];

    for (var i = 0; i < elementsWithTooltips.length; i++) {

        tmpTitles[i] = elementsWithTooltips[i].title;

        elementsWithTooltips[i].tmp_id = i;

        elementsWithTooltips[i].addEventListener("mouseover",function(e) {
            toolTipContainer.innerHTML = this.title;

            this.title = "";
            toolTipContainer.style.left = e.clientX + document.documentElement.scrollLeft + 15 + "px";
            toolTipContainer.style.top = e.clientY + document.documentElement.scrollTop - 15 + "px";
            toolTipContainer.style.display = "block";

        });
        elementsWithTooltips[i].addEventListener("mouseout", function(e) {
            this.title = tmpTitles [this.tmp_id];
            toolTipContainer.innerHTML = this.title;
            toolTipContainer.style.display = "none";

        });
    }
}

window.onload = function () {
    createTooltips();
    var mainImage = document.getElementById("mainImage");
    var image = new Image();

    mainImage.appendChild(image);

    var thumbnails = document.getElementsByClassName("thumbnail");
    var currentThumbnail = thumbnails[0];

    image.src = currentThumbnail.getAttribute("src");
    currentThumbnail.className += " current";

    for (var i = 0; i < thumbnails.length; i++) {

        thumbnails[i].addEventListener("onmouseover", function () {

            currentThumbnail.className = currentThumbnail.className.replace("current", "");
            currentThumbnail = this;
            currentThumbnail.className += " current";

            image.src = this.getAttribute("src");
        });
    }

};










