
function drago(event){
    event.preventDefault();
}
function drop(event){
    event.preventDefault();

    var id = event.dataTransfer.getData('id');
    event.target.appendChild(document.getElementById(id));
}

$(document).ready(function(){
    $('#imgcat').dragstart(function(event){
        event.dataTransfer.setData("id", $(this).attr("id"));
    });
    $('#dropzone').dragover(function(event){
        event.preventDefault();
    }).drop(function(){
        event.preventDefault(event);
        var id = event.dataTransfer.getData('id');
        event.target.appendChild(document.getElementById(id));
    })  
});