$(document).ready(
    function () {
        $(".news").truncate();
    });

function nazwa() {
    return "lalala";
}

(function ($) {
    $.fn.truncate = function (options) {
        var defaults = {
            moreText: " more...",
            lessText: " less..."
        }
        var settings = $.extend({}, defaults, options)
        return this.each(
            function () {
                var actualObj = jQuery(this);
                var message = actualObj.html();
                var messageLength = message.length;
                

                if (messageLength > 400) {
                    var splitLocation = message.indexOf(" ", 400);
                    var visiblePart = message.substring(0, splitLocation);
                    var hiddenPart = message.substring(splitLocation, message.length);

                    actualObj.html("<span class='visiblePart'>" + visiblePart + "</span><span class='moreText'>" + settings.moreText + "</span><span class='hiddenPart'>" +hiddenPart + "</span><span class='lessText'>" + settings.lessText + "</span>")

                    $(".hiddenPart", actualObj).css("display", "none");
                    $(".lessText", actualObj).hide();
                    $(".moreText, .lessText").css("color", "#feffaf")
                    $(".moreText", actualObj).click(
                        function () {
                            $(".hiddenPart", actualObj).toggle()
                            $(".moreText", actualObj).hide();
                            $(".lessText", actualObj).show();
                        }
                    );
                    $(".lessText", actualObj).click(
                        function () {
                            $(".hiddenPart", actualObj).toggle()
                            $(".moreText", actualObj).show();
                            $(".lessText", actualObj).hide();
                        }
                    );
                    $(".moreText, .lessText", actualObj).hover(
                        function () {
                            $(this).css({
                                "color": "yellow",
                                "cursor": "pointer"
                            })
                        }
                    );
                    $(".moreText, .lessText", actualObj).mouseout(
                        function () {
                            $(this).css({
                                "color": "#feffaf",
                                "cursor": "deaful"
                            })
                        }
                    );
                }
            }
        );

    }
})(jQuery);