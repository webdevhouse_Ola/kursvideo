$(document).ready(
    function () {
        rotateImg();      
});

function rotateImg(){
    var firstImg = $(".imgToRotate:first-child");
    firstImg.addClass("current");
    rotate();
}
function rotate(){
    var current = $(".current");

    current.animate({
        "opacity": 0
    },2000 ,function(){
        $(this).removeClass("current")
    });
    current = current.next();
    if(current[0]==undefined)
        current = $(".imgToRotate:first-child");
    current.css("opacity",0).addClass("current").animate({"opacity":1},2000,rotate);
}