/*
$("document").ready(
	function()
	{
		alert("lalala");
	}
);
*/

$("document").ready(

	// function(){
	// 	//$("div.a").css("color","blue");
	// 	//$(".a").css("color","blue");
	// 	//$("div > i").css("color","blue");
	// 	//$("div.a, li b").css("color","blue");
	// 	//$("#wyjatkowy + div").css("color","blue"); //pobiera tagi div ktore są obok id #wyjątkowy

	// 	$("div").click(
	// 		function(){
	// 			//$("div").css("font-size", "2em")
	// 			$(this).css("font-size", "2em")
	// 		}
	// 	).css("color","green");
	// }

	

	function(){
		$("div:first").css("border", "1px solid black"); //last
		//FILTRY PODSTAWOWE
		//$("div:even").css("border", "1px solid black"); //odd
		//$("div:eq(2)").css("border", "1px solid black"); //eq=równy, gt=większy niż lt=mniejszy niż
		//$("div:not(div:eq(2))").css("border", "1px solid black"); // nie taki jak not(selector)
		//$("*:header").css("border", "1px solid black"); // cokolwiek co ma w sobie selektor header

		//FILTRY ZAWARTOŚCI		
		//$("div:contains('słowo')").css("border", "1px solid black"); // posiada(coś)
		//$("div:has('b')").css("border", "1px solid black"); // div w którym jest <b>
		//$("div:parent").css("border", "1px solid black"); // kazdy div który jest rodzicem
		//$("ol li:nth-child(3n+1)").css("border", "1px solid black"); // only-child - jedyne dziecko

		//FILTRY WIDOCZNOŚCI
		//$("div:visible").css("color","teal") //hidden

		//
		//
		//
		//$("img[alt]").css("border", "2px solid black"); //istnieje atrybut o kluczu alt
		//$("img[src='zdjecie1.jpg']").css("border", "2px solid black"); //jest rowny zdjecie1.jpg
		
		//$("img[alt!='jakies zdjecie']").css("border", "2px solid black");//jest rozny od jakies zdjecie
		
		//$("img[src$='.gif']").css("border", "2px solid black"); //konczy sie na .gif
		
		//$("img[src^='zdjecie']").css("border", "2px solid black"); //zaczyna sie od zdjecie
		
		//$("img[src*='.']").css("border", "2px solid black"); //zawiera kropkę
		
		//$("img[alt][src='zdjecie1.jpg']").css("border", "2px solid black"); //wybierz takie obrazki w ktorych istnieje atrybut o kluczu alt i atrybut o kluczu src ma wartosc zdjecie1.jpg
		
	}
);