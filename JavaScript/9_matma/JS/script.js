/*

    Math.round() - zaokrąglenie liczby po całości zgonie z zasadami matematyki
    Math.cell() - zawsze do góry
    Math.floor() - zawsze w dół
    toFixed(ileMiejscaPoPrzecinku)
    Math.abs() - wartość bezwzględna
    Math.pow(x,y) - x^y

    parseInt
    parseFloat
    typeof

    Math.random() - zwraca losową liczbę od 0 do 1
              
              
 */

window.onload = function () {

    /*    var info = document.getElementById("info");
    
        var x = parseFloat("2.12314ks");
    
        info.innerHTML = x.toFixed(3); 
    */
    var cytat = document.getElementById("cytat");
    var autor = document.getElementById("autor");

    var cytaty = [
        "Tekst pierwszy I",
        "Tekst drugi II",
        "Tekst trzeci III",
        "Tekst czwarty IV"];

    var autorzy = [
        "Autor pierwszy I",
        "Autor drugi II",
        "Autor trzeci III",
        "Autor czwarty IV"];

    var losowaLiczba = Math.floor(Math.random()* 4);

    cytat.innerHTML = cytaty[losowaLiczba];
    autor.innerHTML = autorzy[losowaLiczba];




};
