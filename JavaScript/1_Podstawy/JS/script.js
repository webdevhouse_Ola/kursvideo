// /* OPERATORY RELACYJNE - PORÓWNANIA
//     =     operator przypisania
//     ==    porównanie
//     ===   porównuje czy wartości zmiennych są identyczne

//     np
//     var a=3, b="3";
//     alert(a==b); zwrórci TRUE
//     alert(a===b); zwroci FALSE
// */

// // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference

// var x=5,
//     y=6;
// //alert("Wynik dodawania zmiennych x i y wynosi: " + (x + y));

// //
// //

// function test() {
//   return 5;
// }
// //alert("Funkcja zwróciła wartość: " + test());

// //
// //

// function dodawanie(x, y){
//   return parseInt (x + y);
// }
// var sumaZmiennych = dodawanie(2,7);
// alert("Funkcja dodawanie dla wartości 2 i 7 zwróciła wartość: " + sumaZmiennych);


// var a = 5; //ZMIENNA GLOBALNA

// function testZmienne(a){ // ZMIENNA LOKALNA
//   var a; //ZMIENNA LOKALNA
//   a = 5; // ZMIENNA GLOBALNA
// }

// testZmienne(); //ZMIENNA LOKALNA
// alert(a); //ZMIENNA GLOBALNA

// //

// /*FUNKCJE ANONIMOWE*/

// var x = function(){
//   alert("xxxx");
// };
// x();

// function test(f){
//   f(10);
// }
// test(
//   function(x){
//     alert("cosik " + x);
//   }
// );

// //
// //

// var hi = function(type){

//   if(type === "szef")
//     return function(name){
//       alert("Cześć szefie, " + name + " !")
//     };
//     else
//       return function(name){
//         alert("Cześć" + name + " !")
//       };
// };

// var funkcjaZwrocona = hi("szef");

// funkcjaZwrocona("Arek");

// //
// //

// var div = document.getElementById('testID');
// div.innerHTML = "nowa treść";


// // var osoba = {                               //lub var osoba = new Object({taratata});
// //   imie: "Chuje Muje",
// //   nazwisko: "Dzikie Węże",
// //   pobierzInformacje: function(){
// //     return this.imie + " " + this.nazwisko;
// //   },
// //   toString: function(){
// //     return this.imie + " " + this.nazwisko;
// //   }
// // };
// //alert(osoba);

// ///FUNKCJA KONTRUKCYJNA -> KLASA
// function osoba(imie, nazwisko, wiek){
//   this.name = imie;
//   this.surname = nazwisko;
//   this.age = wiek;
//   this.toString = function(){
//     return this.name + " " + this.surname;
//   };
// };

// var x = new osoba("Chuje", "Muje", 69);
// var y = new osoba("Dzikie", "Węże", 666);

// //x.specifiedValue = 12;
// osoba.prototype.specifiedValue = 12;

// alert(x);
// alert(y);

// //
// //

// //ARRAY

// produkty = [                //LUB var produkty2 = new Array();
//   "PHP",
//   "MySQL",
//   "JavaScript"
// ];

// produkty.push("PDO");           //LUB produkty[produkty.length] = "PDO";
// produkty[0];

// //TABLICA ASSOCJACYJNA
// var osoba = [];
// osoba["imie"] = "Asia";

// // var liArray = document.getElementById('kursyTworzeniaStronWWW').getElementsByTagName("li");
// // alert(liArray[0].innerHTML);
// // alert(liArray[1].innerHTML);
// // alert(liArray[2].innerHTML);

// //
// //

// //PĘTLE

// kursyProgramowania = [                //LUB var produkty2 = new Array();
//   "C++",
//   "C#",
//   "Java"
// ];

// var rezultat = document.getElementById("rezultat");

// var i = 0;
// while(i<kursyProgramowania.length){
//   rezultat.innerHTML += kursyProgramowania[i] + "<br>";
//   i++;
// }

// var petlaFor = document.getElementById("petlaFor");

// for(var i=0; i < kursyProgramowania.length; i++){
//   if(i%2 !== 0)
//   kursyProgramowania[i].innerHTML += petlaFor[i] + "parzysty: " + "<br>";
//   else
//     continue;
//   }

//   for(var i=0; i<10; i++){
//     if(i === 4)
//       break
//       alert(i);
//   }

//   //
//   //

//   //PETLA FOR / IN

//   var kursyProgramowania = document.getElementById("kursyProgramowania").getElementsByTagName("li");

//   for(var property in kursyProgramowania){
//     if(typeof(kursyProgramowania[property]) !== "object")
//       continue;
//     alert(kursyProgramowania[property].innerHTML);
//   }


// var kursyProgramowania = document.getElementById("kursyProgramowania");
// kursyProgramowania.childNodes[1].firstChild.textContent = "coś nowego";
// alert(kursyProgramowania.childNodes[1].firstChild.textContent);


// // var kursyTworzeniaStronWWW = document.querySelectorAll("#kursyTworzeniaStronWWW li");
// // for(var index in kursyTworzeniaStronWWW){
// //   kursyTworzeniaStronWWW[index].style.color = "red";
// // }


// //
// //

// var paragraf = document.createElement("p");
// paragraf.style.color = "green";
// paragraf.className = "m-paragraf";
// paragraf.innerHTML = "nowy testk do paragrafu";

// var body = document.querySelector("body");
// body.appendChild(paragraf);

// //
// //

//EVENTY - zdarzenia

// var test = document.getElementById("zdarzenie");
// test.onclick = function(){
//   alert("cośtam");
// }

// //
// //

// window.onload = function(){

//   var testKolor = document.getElementById("zdarzenie");
//   var stop = document.getElementById("stop");
  

//   function zmienKolor(){

//     //this.style.color="red";
//     this.className = "kolorRed";
//   }
  
//   function zmienKolor2(){
  
//     //this.style.color="black";
//     this.className = "";
//   }

//   function powiekszCzcionke(){
//     var fontSize = parseInt(window.getComputedStyle(this).fontSize);
//     this.style.fontSize = (++fontSize) + "px";
//   }

//   function wykonaj(e, str){
//     //var e = event || window.event;
//     var tmp = document.getElementById("tmp");
//     tmp.innerHTML = e.clientX + " " + str;

//     var tooltip = document.getElementById("tooltip");

//     tooltip.style.display="block";
//     tooltip.style.left=e.clientX + 10 + "px";
//     tooltip.style.top=e.clientY + 10 + "px";

//   }
  
  
//   // testKolor.onmouseover = zmienKolor;
//   // testKolor.onmouseout = zmienKolor2;

//   testKolor.addEventListener("mouseover", zmienKolor);
//   testKolor.addEventListener("mouseover", powiekszCzcionke);
//   testKolor.addEventListener("mouseout", zmienKolor2);

//   stop.addEventListener("click", function(){

//     testKolor.removeEventListener("mouseover", powiekszCzcionke);
//   });

// test.onmousemove = function(e){
//   wykonaj(e, this.tagName);
// }

// };

window.onload = function(){

  var email = document.getElementById("email");
  var submitFormButton = document.querySelector("#newsletter, input[type='submit']");

  submitFormButton.onclick = function(e){
    var tmp = document.getElementById("tmp");
    e.preventDefault();
    tmp.innerHTML = email.value;

    this.parentNode.submit();
  };

  var toTopButton = document.getElementById("toTopButton");

  window.onscroll = function(){

    var test = document.getElementById("test");
    var toTopButton = document.getElementById("toTopButton");
    var yScrollAxis = window.pageYOffset

    if(window.pageYOffset > 300){
      toTopButton.style.display="block";
    }
    else{
      toTopButton.style.display="none";
    }

    test.innerHTML = yScrollAxis;
  };

  toTopButton.onclick = function(){

    window.scrollBy(0, -1*pageYOffset);
  };


  //IMAGE

  function movingImage(e, objecttoMove){
    objecttoMove.style.left = e.clientX - objecttoMove.width/2 + "px";
    objecttoMove.style.top = e.clientY - objecttoMove.height/2 + "px";
    
  };

  var wykrzyknik = document.getElementById("wykrzyknik");

  wykrzyknik.onmousedown = function(){

    var self = this;
    document.onmousemove = function(e){
      movingImage(e, self)
    };
  };

  wykrzyknik.onmouseup = function(){
    document.onmousemove = null;
  };

  wykrzyknik.ondragstart = function(e){
    e.preventDefault();
  };

  


};