/*
  new Date(miliseconds) - ilość milisekund od 1 stycznia 1970
  new Date(year, month, day, hour, minute, second, milisecond) - tylko 3 pierwsze sa wymagane
                                                                miesiące liczymy od 0!!!
  new Date(dateString) - gdzie datestring to data podana w formacie stringa zgodnie z normalmi
  np. YYYY-MM-DD, YYYY-MM, MM/DD/YYYY
   
*/

Date.prototype.getMonthPL = function(){
  var months = ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"];

  return months[this.getMonth()];
  
};

function getMonthFromNumber(monthNumber){

  var months = ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"];

  return months[monthNumber];
};

function ileCzasuMinelo(from, to){
  return "s:"+(from - to)/1000 + " min:"+ (from - to)/1000/60 + " h:" +(from - to)/1000/60/60 + " d:" +(from - to)/1000/60/60/24;
};

function Clock(elementHandler){

  this.elementHandler = elementHandler;
  this.acctualDate = new Date();
  this.start = function(){
    this.updateElementHandlerContent();
    var self = this;
    setInterval(function(){ self.addSecond(); self.updateElementHandlerContent(); }, 1000);
  };
  this.addSecond = function(){
    this.acctualDate = new Date();
  };
  this.updateElementHandlerContent = function(){
    this.elementHandler.innerHTML = this.getFormatedDate();
  };

  this.getFormatedDate = function(){
    var hours = this.acctualDate.getHours();
    var minutes= this.acctualDate.getMinutes();
    var seconds = this.acctualDate.getSeconds();

    if(hours < 10)
      hours = "0" + hours;
    if(minutes < 10)
    minutes = "0" + minutes;
    if(seconds < 10)
    seconds = "0" + seconds;


    var suffix = "";
    if(hours < 12)
      suffix="AM";
    else
      suffix="PM";

    return hours + ":" + minutes + ":" + seconds + " " + suffix;
  };
}

window.onload = function(){

  var info = document.getElementById("info");

  var dzis = new Date();
  var jutro = new Date(dzis.getTime() + 1000*60*60*24)
  var pojutrze = new Date("2019-05-12");

  var miesiacPL = getMonthFromNumber(jutro.getMonth());

  info.innerHTML = dzis.getDate() + "." + (dzis.getMonth()+1) + "." + dzis.getFullYear();
  info.innerHTML = dzis.toLocaleString();
  info.innerHTML = dzis.getDay();
  info.innerHTML = jutro.getDay();
  info.innerHTML = pojutrze.toLocaleString();
  info.innerHTML = miesiacPL;
  info.innerHTML = jutro.getMonthPL();

  info.innerHTML = ileCzasuMinelo(jutro, dzis);

  //
  //
  //
      /*ZEGAR NA STRONIE*/

      var zegar = document.getElementById("zegar");

      var clock = new Clock(zegar);

      clock.start();
  
};