/*Tabliczka mnożenia - pętla w pętli */

window.onload = function(){

  var taliczkaMnozenia = "<table>";

  for(var i=1; i<=10; i++){
    taliczkaMnozenia += "<tr>";

    for(var j=1; j<=10; j++){  
      taliczkaMnozenia += "<td>" + i * j + "<td>";
    }
    taliczkaMnozenia += "</tr>";
  }

  taliczkaMnozenia += "</table>";

  var rezultat = document.getElementById("rezultat");

  rezultat.innerHTML = taliczkaMnozenia;

};