module.exports = function (grunt) {
  //require('load-grunt-task')(grunt);
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concurrent:{
      target:{
        task: ["watch:etykietaAutoprefixer"],
        options:{
          logConcurrentOutput: true
        }
      }
    },

    autoprefixer: {
      options: {
        // Task-specific options go here.
      },
      dist: {
        // Target-specific file lists and/or options go here.
        src: 'css/style.css',
        dest: 'css/styleprefixer.css'
      }
    },
    watch: {
      etykietaAutoprefixer: {
        files: 'css/style.css',
        taks: ['autoprefixer']
      }
    },
    cssmin: {
      target: {
        files: {
          'css/styleprefixer.min.css': ['css/styleprefixer.css']
        }
      }
    },
    // uglify: {
    //   target: {
    //     files: {
    //       'js/output.min.css': ['js/javascript1.js', 'js/javascript1.js']
    //     }
    //   }
    // },
    imagemin: {
      dynamic: {
        options: {
          optimizationLevel: 7
        },
        files: [{
          expand: true,
          cwd: 'Img/',
          src: ['**/*.{jpg,png,gif}','!build/**/*.{jpg,png,gif}'],
          dest: 'Img/build'
        }]
      }
    }
  });

  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  //grunt.loadNpmTasks('load-grunt-task');
  grunt.loadNpmTasks('grunt-contrib-imagemin');

  grunt.registerTask("default", ['autoprefixer', 'watch']);
  //grunt.registerTask("minifyNewImages", ['newer: imagemin']);
}