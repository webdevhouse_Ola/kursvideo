/*
   substring/slice - potnij od / do
   substr - tnie od / ILE MA POCIAC
   split - rozdzielić na tablicę string
   join - łączy tablicę elmentów w string
   replace - podmiana
   trim - usunięcie białych znaków z lewj i prawej strony stringa
   lastIndexOf - ostatni indeks elementu w stringu
   indexOf - pierwszy indeks elementu w stringu
   search 
*/

window.onload = function(){

  var info = document.getElementById("info");
   
  info.innerHTML = "To jest jakiś testowy tekst o niczym. ktory właściwie \
                    to nie ma sensu";

  var tmp = "Jak chcemy w cudyslowie mieć znak \" to trzeba dać backslash ";

  info.innerHTML=tmp.charAt(0).toUpperCase() + tmp.slice(1).toLowerCase();

  var link = "https://www.google.pl/imghp";
  //info.innerHTML = link.slice(link.lastIndexOf("/"));

  //
  //
  //

  /*
  REGEXP
    regular expression - regularne wyrażenia

    stringDoPrzeszukania.search(wzór) - szuka i zwraca indeks
    stringDoPrzeszukania.match(wzór) - szuka i zwraca w postaci tablicy
    regExp.exec(stringDoPrzeszukania) - to samo co wyżej tyle, że nie działa flaga global i zwracana jest tylko jedna wartość
    stringDoPrzeszukania.replace(wzór, "naCO"); - podmienia
    wzór.test(stringDoPrzeszukania); - sprawdza, czy po prostu coś takiego się znajduje w stringu


    g - global - po całym stringu
    i - insensitive (nieczuły na wielkość znaków)
  */

  var info2 = document.getElementById("info2");
  var indeksy = "A-56 B-12 K-51 A-53 A43"

  var result = indeksy.match(/A.?[0-9]{1,}/g);

  info2.innerHTML = result;

  //
  //
  //
  //
  //

            //JAK SPRAWDZIĆ CZY HASŁO JEST SILNE

  var info3 = document.getElementById("info3");
  var testButton = document.getElementById("myForm").testButton;
  //var pw = "abc123Bdef";

  testButton.onclick = function(e){

    e.preventDefault();
    var pw = document.getElementById("myForm").pw.value;
    var regExpPattern = /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}/;

    if(regExpPattern.test(pw)){
      document.getElementById("myForm").submit();
    }
    else{
      info3.innerHTML = "Hasło jest za słabe"
    }

  //info3.innerHTML = regExpPattern.test(pw);

  };  
  
};