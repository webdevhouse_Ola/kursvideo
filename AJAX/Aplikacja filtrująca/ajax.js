window.onload = init;
var aktualnyURL = window.location.href;

function init()
{
	setInterval(checkURL, 100);
	showHint(true);
}
function checkURL()
{
	if (aktualnyURL != window.location.href)
	 showHint(true);
}
function ajaxInit() 
{
	var XHR = null;
	
	try 
	{
		XHR = new XMLHttpRequest();
	}
	catch(e)
	{
		try
		{
			XHR = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch(e2)
		{
			try
			{
				XHR = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e3)
			{
				alert("Niestety Twoja przeglądarka nie obsługuje AJAXA");
			}
		}
	}
	
	return XHR;	
}
function showHint(isFirstTime)
{
	var XHR = ajaxInit();
	
	if (XHR != null)
	{
		var query;
		if (!isFirstTime) 
		{
			var powierzchniamin = document.getElementById("powierzchniamin").value;
			var powierzchniamax = document.getElementById("powierzchniamax").value;
			var ludnoscmin = document.getElementById("ludnoscmin").value;
			var ludnoscmax = document.getElementById("ludnoscmax").value;
			
			query = "powierzchniamin=" + powierzchniamin +
			"&powierzchniamax=" +
			powierzchniamax +
			"&ludnoscmin=" +
			ludnoscmin +
			"&ludnoscmax=" +
			ludnoscmax;
			
			window.location.href = "#!" + query;
			
			aktualnyURL = window.location.href;
		}
		else
		{
			var url = window.location.href;
			
			query = url.substr(url.indexOf("#!")+2);
			
			if (url.indexOf("#!") != -1)
			{
				var tmpArray = query.split("&");
				
				document.getElementById("powierzchniamin").value = tmpArray[0].substr(tmpArray[0].indexOf("=")+1);
				document.getElementById("powierzchniamax").value = tmpArray[1].substr(tmpArray[1].indexOf("=")+1);
				document.getElementById("ludnoscmin").value = tmpArray[2].substr(tmpArray[2].indexOf("=")+1);
				document.getElementById("ludnoscmax").value = tmpArray[3].substr(tmpArray[3].indexOf("=")+1);
				
			}
			aktualnyURL = window.location.href;
		}
		
		XHR.open("GET", "wojewodztwa.php?"+query, true);
		
		XHR.onreadystatechange = function()
		{
			if (XHR.readyState == 4)
			{
				if (XHR.status == 200)
				{
					document.getElementById("tekst").innerHTML = XHR.responseText;
				}
				else
				 alert("Wystąpił błąd "+ XHR.status);
			}
		}
		
		XHR.send(null);
		
	}
}
