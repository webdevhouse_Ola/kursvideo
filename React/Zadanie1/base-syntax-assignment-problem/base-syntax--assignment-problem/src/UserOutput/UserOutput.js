import React from 'react';

const userOutput = (props) => {
  const outputStyle = {
    color: 'green'
};
  return (
    <div className="UserOutput">      
      <p style={outputStyle}>Username: {props.userName}</p>
      <p>Paragraf 2</p>
    </div>
)};

export default userOutput;
