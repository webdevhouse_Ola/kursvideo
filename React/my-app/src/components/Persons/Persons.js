import React, { Component } from 'react';
import Person from './Person/Person'
//import ErrorBoundary from '../../ErrorBoundary/ErrorBoundary';

//const persons = (props) => {
class Persons extends Component {

  // static getDrivedStateFromProps(props, state){
  //   console.log('[Persons.js] getDrivedStateFromProps');
  //   return state;
  // }

  // componentWillReceiveProps(props){
  //   console.log('[Persons.js] componentWillReceiveProps', props);
  // }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('[Persons.js] shouldComponentUpdate');
    return true;
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    console.log('[Persons.js] shouldComponentUpdate');
    return { message: 'Snapshot!' };
  }

  // componentWillUpdate(){}

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log('[Persons.js] componentDidUpdate');
    console.log(snapshot);
  }

  componentWillUnmount(){
    console.log('[Persons.js] componentWillUnmount');
  }

  render() {
    console.log('[Persons.js] rendering...');
    return this.props.persons.map((person, index) => {
      return (
        //<ErrorBoundary key={this.person.id}>
        <Person
          click={() => this.props.clicked(index)}
          name={person.name}
          age={person.age}
          changed={(event) => this.props.changed(event, person.id)} />//</ErrorBoundary>)
      )
    });

  }

};
export default Persons;